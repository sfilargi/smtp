FROM debian

RUN apt-get -qq update

RUN apt-get -yqq install locales curl vim build-essential tmux sudo pkg-config \
    libdev-mariadb libpcre3-dev

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    locale-gen
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en 
ENV LC_ALL en_US.UTF-8

WORKDIR /ws

RUN curl -sSL https://get.haskellstack.org/ | sh

ADD . /ws/
