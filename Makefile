.PHONY = build run

PWD := ${shell pwd}

run: build
	docker run -it --rm -v ${PWD}:/ws --name smtp smtp bash

build:
	docker build . --tag smtp

