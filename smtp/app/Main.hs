{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

import Control.Monad.State

import Data.List.Split
import Data.Pool (Pool, createPool, withResource)
import Database.MySQL.Simple (connect, close, query_, Connection, connectHost, connectUser, connectPassword, connectDatabase, defaultConnectInfo, Only)

import Network.Wai
import Network.HTTP.Types
import Network.Wai.Handler.Warp (run)

import qualified Data.Text as T

data Route = Route [String] deriving Show

data Routes = Routes [Route]

class ToJSON a where
    toJSON :: a -> String

instance ToJSON String where
    toJSON s = s

instance (ToJSON a) => ToJSON [a] where
    toJSON l = foldl (++) "" $ map (\x -> toJSON x) l


data MyState s = MyState { myState :: s }

type MyStateM s a = MyState s -> (a, (MyState s))

-- myPut :: s -> MyStateM s ()
-- myPut s =
--     MyState s

myGet :: MyStateM s s
myGet st = 
    ((myState st), st)

myPut :: s -> MyStateM s ()
myPut s st =
    ((), (MyState s))

myBind :: MyStateM s a -> (a -> MyStateM s b) -> MyStateM s b
myBind oldStateM fa =
    (\(a, oldState) -> ((fa a) oldState)) . oldStateM 

-- runMyState :: s -> (MyState s -> MyState s)

runMyState :: MyStateM s () -> s -> s
runMyState m s = 
    let (_, s') = m (MyState s) in 
    myState s'

doSomething :: MyStateM String ()
doSomething = (myPut "Works!") `myBind` \_ -> (myPut "Oh my!")

newRoute :: String -> Route
newRoute path = 
    Route $ filter (\x -> x /= "") $ splitOn "/" path 

newConn = connect defaultConnectInfo 
    { connectHost = "db"
    , connectUser = "root"
    , connectPassword = "secret"
    , connectDatabase = "test" }

getPool = createPool newConn close 1 10 5


app :: Pool Connection -> Application
app pool _ respond = do
    withResource pool $ \c -> query_ c "SELECT 1" :: IO [Only Int]
    respond $ responseLBS
        status200
        [("Content-Type", "text/plain")]
        "Hello, Web"

main :: IO ()
main = do
    putStrLn $ runMyState doSomething ""
    putStrLn $ show $ newRoute "/users///test"
    putStrLn $ "http://localhost:8080/"
    pool <- getPool
    run 8080 $ app $ pool
